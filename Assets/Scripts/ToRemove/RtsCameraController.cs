﻿using UnityEngine;

// Класс, который отвечает за движение камеры
public class RtsCameraController : MonoBehaviour 
{
    public float moveSpeed = 10f;

    void LateUpdate()
    {
        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");

        var movement = new Vector3(horInput, 0, vertInput);
        var yRot = Quaternion.Euler(0, transform.eulerAngles.y, 0);
        movement = yRot * movement * moveSpeed * Time.deltaTime;

        transform.Translate(movement, Space.World);
    }
}
