﻿using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour 
{
	/*public RectTransform selectionRect;
    private GameObject selectionRectGameObject;
    private float selectionRectScale;

    private bool isSelecting;
    private Vector3 mousePosOnSelectionStart;
    private Vector2 fromStartToNext;
    private Vector2 positiveFromStartToNext;

    public static List<Selectable> selectedObjects = new List<Selectable>();
    public static List<Selectable> selectableObjects = new List<Selectable>();
    public Selectable hoveredObject;

    private Camera mainCamera;

    public PlayerController player;

    void Start() 
	{
		mainCamera = Camera.main;
        selectionRectGameObject = selectionRect.gameObject;
        selectionRectScale = selectionRect.localScale.x;
        selectionRectGameObject.SetActive(false);
	}

	void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isSelecting = true;
            mousePosOnSelectionStart = Input.mousePosition;

            selectedObjects.Clear();
            foreach (var selectable in selectableObjects)
            {
                selectable.selectionCircle.enabled = false;
            }

            selectionRectGameObject.SetActive(true);
        } 
        
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        Selectable underMouse = null;
        bool successfullyRaycasted = Physics.Raycast(ray, out hit);
        if (successfullyRaycasted)
        {
            underMouse = hit.transform.gameObject.GetComponent<Selectable>();
        }

        if (Input.GetMouseButtonUp(0))
        {
            selectedObjects.Clear();
            foreach (var selectable in selectableObjects)
            {
                if ((underMouse != null && selectable == underMouse) 
                    || IsWithinSelectionBounds(selectable.gameObject))
                {
                    selectedObjects.Add(selectable);
                }
            }

            selectionRectGameObject.SetActive(false);
            isSelecting = false;
        }

        if (isSelecting)
        {
            foreach (var selectable in selectableObjects)
            {
                selectable.selectionCircle.enabled = 
                    (underMouse != null && selectable == underMouse)
                    || IsWithinSelectionBounds(selectable.gameObject);
            }

            DrawSelectBox();
        } 
        else 
        {   
            if (underMouse != null && underMouse != hoveredObject)
            {
                DisableHovered();
                underMouse.hoverCircle.enabled = true;
                hoveredObject = underMouse;
            } 
            else if (underMouse == null)
            {
                DisableHovered();
            }

            if (successfullyRaycasted && Input.GetMouseButton(1))
            {
                foreach (var selected in selectedObjects)
                {
                    //var unit = selected.gameObject
                    //    .GetComponent<UnitAi>();
                    //if (unit != null && unit.player == player)
                    //{
                        //HandleRightClick(unit, hit);
                    //}
                }
            }
        }
    }

    private void HandleRightClick(ActorBehaviour unit, RaycastHit hit)
    {
        var obj = hit.transform.gameObject;
        if (obj.layer == LayerMask.NameToLayer("Ground"))
        {
            //unit.Target = null;
            //unit.MoveTo(hit.point, 1f);
            return;
        }

        //var targetUnit = obj.GetComponent<UnitController>();
        //if (targetUnit != null && targetUnit != unit)
        //{
            //unit.Target = targetUnit;
        //}
    }

    private void DisableHovered()
    {
        if (hoveredObject != null)
        {
            hoveredObject.hoverCircle.enabled = false;
            hoveredObject = null;
        }
    }

    private void DrawSelectBox()
    {
        Vector3 screenPos1 = mousePosOnSelectionStart;
        Vector3 screenPos2 = Input.mousePosition;

        screenPos1.x -= Screen.width / 2;
        screenPos1.y -= Screen.height / 2;
 
        fromStartToNext.Set(screenPos2.x - mousePosOnSelectionStart.x
            , mousePosOnSelectionStart.y - screenPos2.y);
        positiveFromStartToNext.Set(Mathf.Abs(fromStartToNext.x)
            , Mathf.Abs(fromStartToNext.y));
        selectionRect.sizeDelta = positiveFromStartToNext 
            / selectionRectScale;

        screenPos1.x += (fromStartToNext.x < 0) ? 
            -selectionRect.sizeDelta.x / 2 * selectionRectScale 
            : selectionRect.sizeDelta.x / 2 * selectionRectScale;
        screenPos1.y += (fromStartToNext.y < 0) ? 
            selectionRect.sizeDelta.y / 2 * selectionRectScale 
            : -selectionRect.sizeDelta.y / 2 * selectionRectScale;

        selectionRect.localPosition = screenPos1;
    }

    private bool IsWithinSelectionBounds(GameObject obj)
    {
        if (!isSelecting)
            return false;

        Bounds viewportBounds = Utils.GetViewportBounds(
            mainCamera, mousePosOnSelectionStart, Input.mousePosition);

        return viewportBounds.Contains(mainCamera.WorldToViewportPoint(
            obj.transform.position));
    }*/
}
