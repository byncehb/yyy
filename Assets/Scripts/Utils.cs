﻿using UnityEngine;

public static class Utils
{
    static Texture2D whiteTexture;
    public static Texture2D WhiteTexture
    {
        get
        {
            if(whiteTexture == null)
            {
                whiteTexture = new Texture2D(1, 1);
                whiteTexture.SetPixel(0, 0, Color.white);
                whiteTexture.Apply();
            }
 
            return whiteTexture;
        }
    }

    private static Bounds bounds;

    public static Bounds GetViewportBounds(Camera camera
        , Vector3 screenPosition1, Vector3 screenPosition2)
    {
        Vector3 v1 = Camera.main.ScreenToViewportPoint(screenPosition1);
        Vector3 v2 = Camera.main.ScreenToViewportPoint(screenPosition2);
        var min = Vector3.Min(v1, v2);
        var max = Vector3.Max(v1, v2);
        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;
     
        bounds.SetMinMax(min, max);
        
        return bounds;
    }

    /// Находит первого родителя в иерархии с заданным тегом
    /**
    \param start GameObject, в родителях которого ищем
    \param tag Тег, который ищем
    \return Возвращает ссылку на первый найденный GameObject, либо null
    */
    public static GameObject FindParentWithTag(GameObject start, string tag)
    {
        var parent = start.transform.parent;
        while (parent != null) 
        { 
            if (parent.tag == tag)
            {
                return parent.gameObject;
            }

            parent = parent.transform.parent;
        }

        return null;
    }
}