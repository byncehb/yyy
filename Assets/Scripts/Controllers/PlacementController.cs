﻿using UnityEngine;


/// Класс, занимающийся размещением актора на сцене
public class PlacementController : MonoBehaviour 
{
    // **** Private Fields ****

    /// Ссылка на материал, показывающий доступное для строительства положение
    [SerializeField]
    private Material placeableMaterial;

    /// Ссылка на материал, показывающий недоступное для строительства положение
    [SerializeField]
    private Material implaceableMaterial;

    /// Ссылка на игрока, которым осуществляется управление
    private PlayerController player;

    /// Ссылка на префаб размещаемого объекта
    private GameObject prefab;

    /// Имя префаба
    private string prefabName;

    /// Является ли префаб зданием
    private bool objIsBuilding;

    /// Ссылка на прозрачный объект, отрисовываемый в сцене во время выбора 
    /// размещения
    private GameObject transparentObj;

    /// Ссылки на renderer'ы объекта
    /**
    Нужны для изменения цвета прозрачного объекта, чтобы указывать доступность 
    места для строительства
    */
    private Renderer[] transparentObjRenderers;

    /// Ссылка на актора объекта
    /**
    Нужна для получения радиуса объекта
    */
    private ActorBehaviour transparentObjBeh;

    /// Флаг, где хранится, можно ли установить объект в текущем месте
    private bool objIsPlaceable;

    /** 
    Тут хранятся collider'ы, которые пересекаются с прозрачным объектом при 
    OverlapBoxNonAlloc. Чтобы понять, что здание можно поставить достаточно
    знать, что коллайдер всего один (коллайдер самого прозрачного объекта), 
    поэтому достаточно выделить места только для двух коллайдеров
    */
    private Collider[] overlapBoxColls = new Collider[2];

    /** 
    Тут хранятся collider'ы, которые находятся при бросании луча вверх. 
    Недостаточно знать то, что нет пересекающих наш объект коллайд, нужно также,
    чтобы он не был полностью внутри другого коллайдера. Это и проверяется 
    рейкастом. 
    Чтобы понять, что здание можно поставить достаточно знать, что коллайдер 
    всего один (коллайдер самого прозрачного объекта), поэтому достаточно 
    выделить места только для двух коллайдеров
    */
    private RaycastHit[] raycastUp = new RaycastHit[2];


    // **** Public Methods ****

    /// Метод, инициализирующий сеанс размещения.
    /**
    Вызывается объектом InputController. Передает нужные данные для начала
    отрисовки прозрачного объекта и выбора места.
    \param player Ссылка на игрока, которому будет принадлежать размещаемый 
    объект
    \param prefab Ссылка на префаб размещаемого объекта
    \param prefabName Имя префаба размещаемого объекта
    */
    public void Init(PlayerController player, GameObject prefab
        , string prefabName)
    {
        this.player = player;
        this.prefab = prefab;
        this.prefabName = prefabName;
        objIsBuilding = prefab.GetComponent<BuildingBehaviour>() != null;
        objIsPlaceable = false;
    }

    /// Осуществляет попытку разместить объект в точку raycast'a
    /**
    \param hit Информация о рейкасте из указателя мыши на экране в сцену. 
    Рейкаст должен был быть успешен.
    \return Удалось ли установить объект
    */
    public bool Place(ref RaycastHit hit)
    {
        if (objIsPlaceable)
        {
            ClearTransparent();
            Vector3 point = AlignmentController.Align(hit.point
            , transparentObjBeh.Radius);
            if (!objIsBuilding)
            {
                player.CreateUnit(prefabName, point, Quaternion.identity);

                return true;
            }
            else
            {
                player.CreateBuilding(prefabName, point, Quaternion.identity);

                return true;
            }
        }

        return false;
    }

    /// Сбрасывает все параметры связанные с размещением текущего 
    /// прозрачного объекта и завершает сессию размещения
    public void Clear()
    {
        player = null;
        prefab = null;
        prefabName = null;
        if (transparentObj != null)
        {
            ClearTransparent();
        }
    }

    /// Отрисовывает прозрачный объект и устанавливает флаг, что в этом месте
    /// возможно установить объект
    /**
    \param hit Информация о рейкасте из указателя мыши на экране в сцену. 
    Рейкаст должен был быть успешен.
    */
    public void DrawTransparent(ref RaycastHit hit)
    {
        if (transparentObj == null)
        {
            InitTransparentObj();
        }

        float radius = transparentObjBeh.Radius;
        Vector3 point = AlignmentController.Align(hit.point, radius);
        transparentObj.transform.position = point;

        int layer = 1 << LayerMask.NameToLayer("Placement Collider");

        int nColls = Physics.OverlapBoxNonAlloc(point
            , new Vector3(radius, radius, radius)
            , overlapBoxColls
            , Quaternion.identity
            , layer);

        const float raycastDistance = 20f;
        /* Тут опускаем пониже точку старта луча, потому что почему-то луч не 
         обнаружит пересечение с коллайдером если начинается изнутри коллайдера 
        */
        int nHits = Physics.RaycastNonAlloc(point + 5 * Vector3.down
            , Vector3.up
            , raycastUp
            , raycastDistance
            , layer);

        if (nColls == 1 && nHits == 1)
        {
            if (!objIsPlaceable)
            {
                objIsPlaceable = true;
                foreach (var r in transparentObjRenderers)
                {
                    r.material = placeableMaterial;
                }
            }
        }            
        else
        {
            if (objIsPlaceable)
            {
                objIsPlaceable = false;
                foreach (var r in transparentObjRenderers)
                {
                    r.material = implaceableMaterial;
                }
            }
        }
    }


    // **** Private Methods ****

    /// Удаляет инстанс прозрачного объекта и затирает информацию о нем
    private void ClearTransparent()
    {
        Destroy(transparentObj);
        transparentObj = null;
        transparentObjRenderers = null;
    }

    /// Создает по префабу прозрачный объект
    private void InitTransparentObj()
    {
        transparentObj = new GameObject();
        transparentObj.name = "Transparent Object";
        transparentObj.transform.parent = transform;

        foreach (Transform t in prefab.transform)
        {
            if (t.name == "Rendered Part")
            {
                Instantiate(t.gameObject, transparentObj.transform);
            }
            else if (t.name == "Bounding Box")
            {
                Instantiate(t.gameObject, transparentObj.transform);
            }
        }

        transparentObjBeh = prefab.GetComponent<ActorBehaviour>();

        foreach (Behaviour component in transparentObj
            .GetComponentsInChildren<Behaviour>())
        {
            component.enabled = false;
        }

        transparentObjRenderers = transparentObj
            .GetComponentsInChildren<Renderer>();
        foreach (var r in transparentObjRenderers)
        {
            r.enabled = true;
            r.material = implaceableMaterial;
        }
    }
}
