﻿using UnityEngine;

public class CameraController : MonoBehaviour 
{
    private Camera _camera;

    [SerializeField]
    private float moveSpeed = 20f;

    public void Init(Camera camera)
    {
        _camera = camera;
    }

    public void HandleCameraMovement (float hor, float vert) 
    {
        var movement = new Vector3(hor, 0, vert);
        var yRot = Quaternion.Euler(0, _camera.transform.eulerAngles.y, 0);

        // Не нужно ли убрать Time.deltaTime в параметр?
        movement = yRot * movement * moveSpeed * Time.deltaTime;


        _camera.transform.Translate(movement, Space.World);
    }
}
