using UnityEngine;
using UnityEngine.EventSystems;

// Класс, который 
// - обрабатывает и направляет камере ввод с клавы и мыши
// - поддерживает выделение юнитов
// - поддерживает контролирование юнитов
// - поддерживает строительство
[RequireComponent(typeof(CameraController))]
[RequireComponent(typeof(PlacementController))]
[RequireComponent(typeof(SelectionController))]
public class InputController : MonoBehaviour 
{
    [SerializeField]
    private CameraController cameraController;

    [SerializeField]
    private PlacementController placementController;

    [SerializeField]
    private SelectionController selectionController;

    [SerializeField]
    public PlayerController player;

    [SerializeField]
    private GameController gameController;

    [System.Serializable]
    private enum Mode { Placement, Selection };

    [SerializeField]
    private Mode mode;

    private RaycastHit hit;
    private Ray ray;

    void Start()
    {
        cameraController = GetComponent<CameraController>();
        cameraController.Init(Camera.main);
        
        placementController = GetComponent<PlacementController>();
        selectionController = GetComponent<SelectionController>();

        if (player == null)
        {
            player = GameObject.Find("Player")
                .GetComponent<PlayerController>();
        }

        if (gameController == null)
        {
            gameController = GameObject.FindWithTag("GameController")
                .GetComponent<GameController>();
        }

        StartSelection();
    }

    void Update()
    {
        HandleInputToCamera();

        switch (mode)
        {
            case Mode.Placement:
                HandlePlacementMode();
                break;
            case Mode.Selection:
                HandleSelectionMode();
                break;
        }
    }

    void LateUpdate()
    {
        cameraController.HandleCameraMovement(Input.GetAxis("Horizontal")
            , Input.GetAxis("Vertical"));
    }

    // Вызывается по нажатию кнопки строительства здания     
    public void StartPlacement(string prefabName, bool isBuilding = false)
    {
        if (mode == Mode.Selection)
        {
            selectionController.Clear();
        }
        else if (mode == Mode.Placement)
        {
            placementController.Clear();
        }

        if (player != null && gameController != null)
        {
            GameObject prefab = gameController.GetPrefab(prefabName
                , isBuilding);
            if (prefab != null)
            {
                placementController.Init(player, prefab, prefabName);

                mode = Mode.Placement;
            }
        }
        else
        {
            StartSelection();
        }
    }

    private void StartSelection()
    {
        mode = Mode.Selection;
        selectionController.Init(player, Camera.main);
    }

    private void HandleInputToCamera()
    {

    }

    private void HandlePlacementMode()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(1) 
                && !EventSystem.current.IsPointerOverGameObject())
        {
            placementController.Clear();
            StartSelection();
        }
        else if (Physics.Raycast(ray, out hit, Mathf.Infinity
            , 1 << LayerMask.NameToLayer("Ground")))
        {
            if (Input.GetMouseButtonDown(0) 
                && !EventSystem.current.IsPointerOverGameObject())
            {
                if (placementController.Place(ref hit))
                {
                    placementController.Clear();
                    StartSelection();
                }
                else
                {
                    placementController.DrawTransparent(ref hit);
                }
            }
            else
            {
                placementController.DrawTransparent(ref hit);
            }
        }
    }

    private void HandleSelectionMode()
    {
        if (Input.GetMouseButtonDown(0) 
                && !EventSystem.current.IsPointerOverGameObject())
        {
            selectionController.StartSelection(Input.mousePosition);
        }

        int groundLayer = LayerMask.NameToLayer("Ground");
        int layerMask = (1 << LayerMask.NameToLayer("Selection Collider"))
            | (1 << groundLayer);
        Selectable underMouse = null;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        bool successfullyRaycasted = Physics.Raycast(ray, out hit, 
            Mathf.Infinity, layerMask);
        if (successfullyRaycasted)
        {
            if (hit.transform.gameObject.layer != groundLayer)
            {
                underMouse = hit.transform.gameObject
                    .GetComponent<Selectable>();
            }
        }


        if (Input.GetMouseButtonUp(0) 
                && !EventSystem.current.IsPointerOverGameObject())
        {
            selectionController.EndSelection(underMouse, Input.mousePosition);
        }

        if (selectionController.IsSelecting)
        {
            selectionController.HandleSelection(underMouse
                , Input.mousePosition);
        }
        else
        {
            selectionController.HandleHover(underMouse);

            if (successfullyRaycasted && Input.GetMouseButton(1) 
                && !EventSystem.current.IsPointerOverGameObject())
            {
                selectionController.HandleRightClick(ref hit);
            }
        }
    }
}
