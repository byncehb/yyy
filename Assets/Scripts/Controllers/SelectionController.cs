﻿using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour 
{
    //private PlayerController player;

    private static List<Selectable> selectedObjects = new List<Selectable>();
    private static List<Selectable> selectableObjects = new List<Selectable>();
    private Selectable hoveredObject;

    private Camera _camera;

    [SerializeField]
    private RectTransform selectionRect;

    private GameObject selectionRectGameObject;
    private float selectionRectScale;

    public bool IsSelecting { get; private set; }

    private Vector3 mousePosOnSelectionStart;
    private Vector2 fromStartToNext;
    private Vector2 positiveFromStartToNext;

    void Start()
    {
        selectionRectGameObject = selectionRect.gameObject;
        selectionRectScale = selectionRect.localScale.x;
        selectionRectGameObject.SetActive(false);
    }

    public void Init(PlayerController player, Camera camera)
    {
        //this.player = player;
        _camera = camera;
    }

    public void Clear()
    {
        //player = null;
        _camera = null;

        selectedObjects.Clear();
        foreach (var selectable in selectableObjects)
        {
            selectable.SelectionCircle.enabled = false;
        }
    }

    public void HandleHover(Selectable underMouse)
    {
        if (underMouse != null && underMouse != hoveredObject)
        {
            DisableHovered();
            underMouse.HoverCircle.enabled = true;
            hoveredObject = underMouse;
        } 
        else if (underMouse == null)
        {
            DisableHovered();
        }
    }

    public void DisableHovered()
    {
        if (hoveredObject != null)
        {
            hoveredObject.HoverCircle.enabled = false;
            hoveredObject = null;
        }
    }

    public void StartSelection(Vector3 mousePos)
    {
        IsSelecting = true;
        mousePosOnSelectionStart = mousePos;

        selectedObjects.Clear();
        foreach (var selectable in selectableObjects)
        {
            selectable.SelectionCircle.enabled = false;
        }

        selectionRectGameObject.SetActive(true);
    }

    public void HandleSelection(Selectable underMouse, Vector3 mousePos)
    {
        foreach (var selectable in selectableObjects)
        {
            selectable.SelectionCircle.enabled = 
                (underMouse != null && selectable == underMouse)
                || IsWithinSelectionBounds(selectable.gameObject, mousePos);
        }

        DrawSelectBox(mousePos);
    }

    public void EndSelection(Selectable underMouse, Vector3 mousePos)
    {
        selectedObjects.Clear();
        foreach (var selectable in selectableObjects)
        {
            if ((underMouse != null 
                && selectable == underMouse) 
                || IsWithinSelectionBounds(selectable.gameObject, mousePos))
            {
                selectedObjects.Add(selectable);
            }
        }

        selectionRectGameObject.SetActive(false);
        IsSelecting = false;
    }

    private bool IsWithinSelectionBounds(GameObject obj, Vector3 mousePos)
    {
        if (!IsSelecting)
            return false;

        Bounds viewportBounds = Utils.GetViewportBounds(
            _camera, mousePosOnSelectionStart, mousePos);

        return viewportBounds.Contains(_camera.WorldToViewportPoint(
            obj.transform.position));
    }

    private void DrawSelectBox(Vector3 mousePos)
    {
        Vector3 screenPos1 = mousePosOnSelectionStart;
        Vector3 screenPos2 = mousePos;

        screenPos1.x -= Screen.width / 2;
        screenPos1.y -= Screen.height / 2;
 
        fromStartToNext.Set(screenPos2.x - mousePosOnSelectionStart.x
            , mousePosOnSelectionStart.y - screenPos2.y);
        positiveFromStartToNext.Set(Mathf.Abs(fromStartToNext.x)
            , Mathf.Abs(fromStartToNext.y));
        selectionRect.sizeDelta = positiveFromStartToNext 
            / selectionRectScale;

        screenPos1.x += (fromStartToNext.x < 0) ? 
            -selectionRect.sizeDelta.x / 2 * selectionRectScale 
            : selectionRect.sizeDelta.x / 2 * selectionRectScale;
        screenPos1.y += (fromStartToNext.y < 0) ? 
            selectionRect.sizeDelta.y / 2 * selectionRectScale 
            : -selectionRect.sizeDelta.y / 2 * selectionRectScale;

        selectionRect.localPosition = screenPos1;
    }

    public void HandleRightClick(ref RaycastHit hit)
    {
        Debug.Log("Selection Controller is handling right cick now");
    }

    public static void RegisterSelectable(Selectable selectable)
    {
        selectableObjects.Add(selectable);
    }

    public static void DeregisterSelectable(Selectable selectable)
    {
        selectableObjects.Remove(selectable);
    }
}
