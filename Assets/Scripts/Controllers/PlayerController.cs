using UnityEngine;


/// Класс, который отвечает за представление данных об игроке внутри игровой 
/// механики
public class PlayerController : MonoBehaviour 
{
    [SerializeField]
    private int id;
    public int Id
    {
        get { return id; }
    }

    [SerializeField]
    private GameController gameController;

    private static int lastId;

    [SerializeField] 
    public GameObject enemyCastle;

    public bool EnemyCastleIsUp { get; set; }
    public Vector3 EnemyCastlePosition { get; private set; }

    /// Цвет юнитов и зданий игрока
    public Color Color { get; private set; }

    void Awake()
    {
        id = lastId;
        lastId++;

        Color = GetColor(id);

        if (enemyCastle == null)
        {
            var castles = GameObject.FindGameObjectsWithTag("Castle");
            foreach (var castle in castles)
            {
                var actor = castle.GetComponent<ActorBehaviour>();
                if (actor.Player != this)
                {
                    enemyCastle = castle;
                    break;
                }
            }
        }

        if (enemyCastle != null)
        {
            EnemyCastleIsUp = true;
            EnemyCastlePosition = enemyCastle.transform.position;
        }
        else
        {
            EnemyCastleIsUp = false;
        }

        if (gameController == null)
        {
            var found = GameObject.FindGameObjectWithTag("GameController");
            gameController = found.GetComponent<GameController>();
        }
    }

    public void Init(GameController controller)
    {
        if (controller == null)
        {
            var found = GameObject.FindGameObjectWithTag("GameController");
            controller = found.GetComponent<GameController>();
        }

        gameController = controller;
    }

    public void CreateUnit(string name, Vector3 position, Quaternion rotation)
    {
        gameController.CreateUnit(name, position, rotation, this);
    }

    public void CreateBuilding(string name, Vector3 position, Quaternion rotation)
    {
        gameController.CreateBuilding(name, position, rotation, this);
    }

    public bool IsAlliedTo(PlayerController other)
    {
        return gameController.AreAllies(this, other);
    }


    // Private Methods

    private Color GetColor(int t_id)
    {
        Color[] colors = { 
            Color.red, Color.green, Color.gray, Color.cyan, Color.blue
            , Color.grey, Color.magenta, Color.black, Color.white
            , Color.yellow 
        };

        return colors[t_id % colors.Length];
    }
}
