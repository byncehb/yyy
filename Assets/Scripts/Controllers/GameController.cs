using System.Collections.Generic;
using UnityEngine;

// Класс, который отвечает за хранение префабов и их инстанциирование
public class GameController : MonoBehaviour 
{
    private readonly Dictionary<string, UnitBehaviour> unitPrefabsDict 
        = new Dictionary<string, UnitBehaviour>();

    private readonly Dictionary<string, BuildingBehaviour> buildingPrefabsDict 
        = new Dictionary<string, BuildingBehaviour>();

    [SerializeField]
    private UnitBehaviour[] unitPrefabs;

    [SerializeField]
    private BuildingBehaviour[] buildingPrefabs;

    [SerializeField]
    private GameObject gameWorld;

    [SerializeField]
    private bool[,] areAllies;

    void Start () 
    {
        foreach (var prefab in unitPrefabs)
        {
            unitPrefabsDict.Add(prefab.name, prefab);
        }

        foreach (var prefab in buildingPrefabs)
        {
            buildingPrefabsDict.Add(prefab.name, prefab);
        }

        if (gameWorld == null)
        {
            gameWorld = GameObject.Find("Game World");
        }

        if (areAllies == null)
        {
            // Инициализируем матрицу союзников по умолчанию
            PlayerController[] players = 
                GameObject.FindObjectsOfType<PlayerController>();
            areAllies = new bool[players.Length, players.Length];
            for (int i = 0; i < players.Length; i++)
            {
                areAllies[i, i] = true;
            }
        }
    }

    public UnitBehaviour CreateUnit(string name, Vector3 position
        , Quaternion rotation, PlayerController player)
    {
        var prefab = unitPrefabsDict[name];
        var unit = Instantiate<UnitBehaviour>(prefab, position, rotation
            , player.transform);
        unit.Init(player);
        return unit;
    }

    public BuildingBehaviour CreateBuilding(string name, Vector3 position
        , Quaternion rotation, PlayerController player)
    {
        var prefab = buildingPrefabsDict[name];
        var building = Instantiate<BuildingBehaviour>(prefab, position, rotation
            , player.transform);
        building.Init(player);
        return building;
    }

    public List<string> GetUnitNames()
    {
        return new List<string>(unitPrefabsDict.Keys);
    }

    public List<string> GetBuildingNames()
    {
        return new List<string>(buildingPrefabsDict.Keys);
    }

    public GameObject GetPrefab(string name, bool isBuilding)
    {
        ActorBehaviour prefab;
        if (isBuilding)
        {
            prefab = buildingPrefabsDict[name];
        }
        else
        {
            prefab = unitPrefabsDict[name];
        }

        return prefab == null ? null : prefab.gameObject;
    }

    public bool AreAllies(PlayerController left, PlayerController right)
    {
        return areAllies[left.Id, right.Id];
    }
}
