﻿using UnityEngine;


/// Класс, реализующий поддержку размещения объектов по сетке
public static class AlignmentController
{
    // **** Public Methods ****

    /// Возвращает новую точку так, чтобы объемлющий коллайдер актора, стоящего
    /// на новой точке, был выровнян по сетке целых чисел
    /**
    \param point Точка, относительно которой находится новая точка
    \param radius Радиус актора
    */
    public static Vector3 Align(Vector3 point, float radius)
    {
        var radiusOffset = new Vector3(radius, 0f, radius);
        Vector3 topLeftPoint = point + radiusOffset;
        var alignedTopLeftPoint = new Vector3(Mathf.Round(topLeftPoint.x)
            , topLeftPoint.y, Mathf.Round(topLeftPoint.z));
        return alignedTopLeftPoint - radiusOffset;
    }
}
