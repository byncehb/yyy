using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour 
{
    [SerializeField]
    private GameController gameController;

    [SerializeField]
    private InputController inputController;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private GameObject buildingButton;

    [SerializeField]
    private GameObject unitButton;

    void Start()
    {
        InitUnitButtons();
        InitBuildingButtons();

        if (inputController == null)
        {
            inputController = FindObjectOfType<InputController>();
        }
    }

    private void InitBuildingButtons()
    {
        float xPadding = buildingButton.transform.position.x;
        float xButtonSize = buildingButton.GetComponent<RectTransform>()
            .sizeDelta.x;
        List<string> buildingNames = gameController.GetBuildingNames();
        for (int i = 0; i < buildingNames.Count; i++)
        {
            GameObject newButton;
            if (i == 0)
            {
                newButton = buildingButton;
            }
            else
            {
                newButton = Instantiate(buildingButton, gameObject.transform);
                newButton.transform.position += 
                    new Vector3((xPadding + xButtonSize) * i, 0, 0);
            }

            newButton.SetActive(true);
            string buildingName = buildingNames[i];
            newButton.name = "Create " + buildingName + " Button";
            newButton.GetComponentInChildren<Text>().text = buildingName;
            newButton.GetComponent<Button>().onClick.AddListener(
                () => inputController.StartPlacement(buildingName, true));
        }
    }

    private void InitUnitButtons()
    {
        float xPadding = unitButton.transform.position.x;
        float xButtonSize = unitButton.GetComponent<RectTransform>()
            .sizeDelta.x;
        List<string> unitNames = gameController.GetUnitNames();
        for (int i = 0; i < unitNames.Count; i++)
        {
            GameObject newButton;
            if (i == 0)
            {
                newButton = unitButton;
            }
            else
            {
                newButton = Instantiate(unitButton, gameObject.transform);
                newButton.transform.position += 
                    new Vector3((xPadding + xButtonSize) * i, 0, 0);
            }

            newButton.SetActive(true);
            string unitName = unitNames[i];
            newButton.name = "Create " + unitName + " Button";
            newButton.GetComponentInChildren<Text>().text = unitName;
            newButton.GetComponent<Button>().onClick.AddListener(
                () => inputController.StartPlacement(unitName));
        }
    }

    public void ChangePlayerButton_OnClick()
    {
        foreach (var p in GameObject.FindGameObjectsWithTag("Player"))
        {
            var pc = p.GetComponent<PlayerController>();
            if (pc != null && pc != inputController.player)
            {
                inputController.player = pc;

                return;
            }
        }
    }
}
