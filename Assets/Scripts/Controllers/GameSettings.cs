﻿using UnityEngine;


// Статический (синглтон) класс, хранящий настройки игры
public class GameSettings : MonoBehaviour 
{
    // **** Singleton Implementation ****

    private static volatile GameSettings instance;
    private static object _lock = new object();

    private GameSettings() { }

    /// Единственная точка доступа к единственному экземпляру класса
    public static GameSettings Instance 
    {
        get 
        {
            if (instance == null) 
            {
                lock(_lock) 
                {
                    if (instance == null) 
                    {
                        instance = FindObjectOfType<GameSettings>();
                    }
                }
            }

            return instance;
        }
    }

    // ААААААА Сложна реализовывать всё это.
    /*
    // **** Public Properties

    /// Отрисовывать ли круг атаки актора только, когда актор выбран
    public bool DrawAttackCircleOnlyWhenSelected
    {
        get { return drawAttackCircleOnlyWhenSelected; }
    }

    [SerializeField]
    private bool drawAttackCircleOnlyWhenSelected;

    /// Отрисовывать ли круг вижена актора только, когда актор выбран
    public bool DrawVisionCircleOnlyWhenSelected
    {
        get { return drawVisionCircleOnlyWhenSelected; }
    }

    [SerializeField]
    private bool drawVisionCircleOnlyWhenSelected;


    // **** Unity Methods ****

    /// Метод Unity
    void OnValidate()
    {
        foreach (var actor in FindObjectsOfType<ActorBehaviour>())
        {
            actor.OnValidate();
            var attacker = actor.GetComponent<Attacker>();
            if (attacker != null)
            {
                attacker.OnValidate();
            }
        }
    }*/
}
