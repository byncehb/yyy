﻿public class GoToCastleStateWarrior : IState<WarriorBehaviour> 
{
    private static volatile GoToCastleStateWarrior instance;
    private static object _lock = new object();

    private GoToCastleStateWarrior() { }

    public static GoToCastleStateWarrior Instance 
    {
        get {
            if (instance == null) 
            {
                lock(_lock) 
                {
                    if (instance == null) 
                    {
                        instance = new GoToCastleStateWarrior();
                    }
                }
            }

            return instance;
        }
    }

    public void Enter(WarriorBehaviour obj)
    {
        obj.SetTarget(obj.Player.EnemyCastlePosition);
    }

    public void Execute(WarriorBehaviour obj)
    {
        ActorBehaviour unit = obj.GetTargetOfAttack();
        if (unit != null)
        {
            obj.Fsm.ChangeState(GoToTargetStateWarrior.Instance);
        }
        else if (!obj.Player.EnemyCastleIsUp)
        {
            obj.Fsm.ChangeState(IdleStateWarrior.Instance);
        }
    }
    
    public void Exit(WarriorBehaviour obj)
    {
        obj.EraseTarget();
    }
}
