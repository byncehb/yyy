﻿public class AttackStateWarrior : IState<WarriorBehaviour>
{
    private static volatile AttackStateWarrior instance;
    private static object _lock = new object();

    private AttackStateWarrior() { }

    public static AttackStateWarrior Instance 
    {
        get {
            if (instance == null) 
            {
                lock(_lock) 
                {
                    if (instance == null) 
                    {
                        instance = new AttackStateWarrior();
                    }
                }
            }

            return instance;
        }
    }

    public void Enter(WarriorBehaviour obj)
    {
        obj.ControlOnlyRotation = true;
        obj.StartAttack();
    }

    public void Execute(WarriorBehaviour obj)
    {
        if (obj.TargetIsAttackable())
        {
            if (obj.IsInRangeToAttack())
            {
                obj.TryToAttack();
            }
            else
            {
                obj.Fsm.ChangeState(GoToTargetStateWarrior.Instance);
            }
        }
        else
        {
            obj.Fsm.ChangeState(IdleStateWarrior.Instance);
        }
    }

    public void Exit(WarriorBehaviour obj)
    {
        obj.ControlOnlyRotation = false;
        obj.StopAttack();
    }
}
