public class IdleStateWarrior : IState<WarriorBehaviour>
{
    private static volatile IdleStateWarrior instance;
    private static object _lock = new object();

    private IdleStateWarrior() { }

    public static IdleStateWarrior Instance 
    {
        get {
            if (instance == null) 
            {
                lock(_lock) 
                {
                    if (instance == null) 
                    {
                        instance = new IdleStateWarrior();
                    }
                }
            }

            return instance;
        }
    }

    public void Enter(WarriorBehaviour obj)
    {
        obj.EraseTarget();
    }

    public void Execute(WarriorBehaviour obj)
    {
        ActorBehaviour unit = obj.GetTargetOfAttack();
        if (unit != null)
        {
            obj.Fsm.ChangeState(GoToTargetStateWarrior.Instance);
        }
        else if (obj.Player.EnemyCastleIsUp)
        {
            obj.Fsm.ChangeState(GoToCastleStateWarrior.Instance);
        }
    }
    
    public void Exit(WarriorBehaviour obj)
    {

    }
}
