﻿public class GoToTargetStateWarrior : IState<WarriorBehaviour>
{
    private static volatile GoToTargetStateWarrior instance;
    private static object _lock = new object();

    private GoToTargetStateWarrior() { }

    public static GoToTargetStateWarrior Instance 
    {
        get {
            if (instance == null) 
            {
                lock(_lock) 
                {
                    if (instance == null) 
                    {
                        instance = new GoToTargetStateWarrior();
                    }
                }
            }

            return instance;
        }
    }

    public void Enter(WarriorBehaviour obj)
    {

    }

    public void Execute(WarriorBehaviour obj)
    {
        ActorBehaviour unit = obj.GetTargetOfAttack();
        if (unit != null)
        {
            obj.MoveToTarget();
            if (obj.IsInRangeToAttack())
            {
                obj.Fsm.ChangeState(AttackStateWarrior.Instance);
            }
        }
        else
        {
            obj.Fsm.ChangeState(IdleStateWarrior.Instance);
        }
    }
    
    public void Exit(WarriorBehaviour obj)
    {
        
    }
}
