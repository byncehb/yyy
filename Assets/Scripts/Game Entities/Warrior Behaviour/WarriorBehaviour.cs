﻿using UnityEngine;


/// Описывает юнита способного атаковать
[RequireComponent(typeof(Attacker))]
public class WarriorBehaviour : UnitBehaviour 
{
    // **** Public Properties ****

    /// Ссылка на конечный автомат, с помощью которого задается поведение юнита
    public StateMachine<WarriorBehaviour> Fsm { get; private set; }


    // **** Private Fields ****

    /// Текущая выбранная цель для атаки
    [SerializeField] 
    private ActorBehaviour targetOfAttack;

    /// Упорядоченный набор номеров слоев (LayerMask) для атаки.
    /**
    При выборе новой цели существует приоритет. Чем раньше слой встречается в 
    массиве, тем выше его приоритет
    */
    private int[] layersOrdered;

    /// Номер слоя текущей цели
    private int nTargetLayer;

    /// Ссылка на компонент, отвечающий за непосредственно атаку
    private Attacker attacker;


    // **** Public Methods ****

    /// Обновляет цель для атаки
    /** 
    Обновляет цель, если появилась более приоритетная. Обновлять цель, если 
    текущая мертва или null. В случае если поблизости нет подходящих целей 
    устанавливает null
    \return Возвращает цель после всех обновлений
    */
    public ActorBehaviour GetTargetOfAttack()
    {
        if (!TargetIsAttackable())
        {
            nTargetLayer = layersOrdered.Length;
            targetOfAttack = null;
        }
        
        FindTargetOfAttack(nTargetLayer);

        return targetOfAttack;
    }

    /// Указывает юниту двигаться к цели атаки
    public void MoveToTarget()
    {
        SetTarget(targetOfAttack);
    }

    /// Достаточно ли близко юнит к цели, чтобы производить атаку
    /**
    Если юнит уже начал замах оружием, но цель отошла за радиус атаки, то 
    считается, что юнит всё еще может продолжать замах, пока цель не уйдет
    слишком далеко. В данном случае за полтора радиуса
    */
    public bool IsInRangeToAttack()
    {
        float reducedDistance = CalculateProperDistanceToTarget();
        float range = attacker.Range;

        if (attacker.IsBackswinging)
        {
            range *= 1.5f;
        }

        return reducedDistance < range;
    }

    /// Возможно ли вообще этому юниту атаковать цель
    public bool TargetIsAttackable()
    {
        return targetOfAttack != null 
            && !targetOfAttack.Destructible.IsDead
            && !Player.IsAlliedTo(targetOfAttack.Player);
    }

    /// Начинает цикл атаки (замах)
    public void StartAttack()
    {
        attacker.Attacking = true;
    }

    /// Прерывает цикл атаки (замах)
    public void StopAttack()
    {
        attacker.Attacking = false;
    }

    /// Осуществляет попытку произвести атаку по цели
    public void TryToAttack()
    {
        if (attacker.ReadyToDealDamage())
        {
            attacker.DealDamage(targetOfAttack.Destructible);
        }
    }


    // **** Unity Methods ****

    /// Метод Unity
    protected override void Start()
    {
        base.Start();

        layersOrdered = new int[] 
        { 
            LayerMask.NameToLayer("Unit")
            , LayerMask.NameToLayer("Building") 
        };

        nTargetLayer = layersOrdered.Length;

        attacker = GetComponent<Attacker>();
        StoppingDistance = attacker.Range;

        Fsm = new StateMachine<WarriorBehaviour>(this
            , IdleStateWarrior.Instance);
    }

    /// Метод Unity
    /**
    Передает управление юнитом конечному автомату
    */
    protected override void Update()
    {
        base.Update();
        
        Fsm.Update();

        var s = Fsm.CurrentState;
        if (s is IdleStateWarrior)
        {
            state = "idle";
        }
        else if (s is GoToCastleStateWarrior)
        {
            state = "go to castle";
        }
        else if (s is GoToTargetStateWarrior)
        {
            state = "go to target";
        }
        else if (s is AttackStateWarrior)
        {
            state = "attack";
        }
    }

    public string state;

    // **** Private Methods ****

    /// Пытается найти более приоритетную цель для атаки
    /**
    \param nLastLayer Номер слоя, которому принадлежит текущая цель. Передается,
    чтобы не рассматривать цели на менее приоритетных слоях
    */
    private void FindTargetOfAttack(int nLastLayer)
    {
        for (int i = 0; i < nLastLayer; i++)
        {
            var target = FindTargetOfAttackOnLayer(layersOrdered[i]);
            if (target != null)
            {
                nTargetLayer = i;
                targetOfAttack = target;
                return;
            }
        }
    }

    /// Пытается найти цель для атаки на конкретном слое
    /**
    \param layer Номер слоя, на котором ищем цель
    */
    private ActorBehaviour FindTargetOfAttackOnLayer(int layer)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position
            , VisionRange, 1 << layer);
        ActorBehaviour closest = null;
        float minSqrDistance = float.MaxValue;
        foreach (var coll in colliders)
        {
            var unit = coll.GetComponent<ActorBehaviour>();
            if (unit != null 
                && unit != this 
                && !Player.IsAlliedTo(unit.Player))
            {
                float sqrDistance = 
                    (coll.transform.position - transform.position).sqrMagnitude;
                if (sqrDistance < minSqrDistance)
                {
                    closest = unit;
                    minSqrDistance = sqrDistance;
                }
            }
        }

        return closest;
    }

    /// Вычисляет расстояние до цели с учетом радиусов юнита и цели
    private float CalculateProperDistanceToTarget()
    {
        float targetRadius = targetOfAttack.Radius;
        float distance = Vector3.Distance(transform.position 
            , targetOfAttack.transform.position);

        return distance - targetRadius;
    }
}