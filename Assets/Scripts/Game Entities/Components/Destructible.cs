﻿using UnityEngine;


/// Класс-компонента, отвечающая за способность объекта получать урон и умирать
/**
Определяет сущность, которую другие акторы могут атаковать
*/
public class Destructible : MonoBehaviour 
{
    // **** Public Properties ****
    
    /// Текущее здоровье
    public float Health
    {
        get { return health; }
        set { health = value < 0 ? 0 : 
            (value > MaxHealth ? MaxHealth : value); }
    }

    [SerializeField] 
    private float health = 100;

    /// Максимальное здоровье
    public float MaxHealth
    {
        get { return maxHealth; }
    }

    [SerializeField]
    private float maxHealth = 100;

    /// Мертв ли объект
    public bool IsDead { get; private set; }


    // **** Public Methods ****

    /// Получение урона объектом
    /**
    Вычисляет сколько урона нанесется объекту и определяет умрет ли объект в 
    результате    
    \param damage Наносимый урон
    */
	public void TakeDamage(float damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Die();
        }
    }


    // **** Private Methods ****

    /// Выполняет действия "смерти" объекта
    private void Die()
    {
        IsDead = true;
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
