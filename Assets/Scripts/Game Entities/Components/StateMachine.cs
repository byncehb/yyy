﻿using UnityEngine;

public class StateMachine<T> 
{
    private readonly T owner;

    public IState<T> CurrentState 
    {
        get;
        set;
    }

    public StateMachine(T owner, IState<T> currentState) 
    {
        this.owner = owner;
        CurrentState = currentState;
    }

    public void Update() {
        if (CurrentState != null) 
            CurrentState.Execute(owner);
    }

    public void ChangeState(IState<T> state) 
    {
        if (state == null) 
        {
            Debug.LogError("<StateMachine::ChangeState>: " + 
                "Trying to change to a null state");
            return;
        }

        CurrentState.Exit(owner);
        CurrentState = state;
        CurrentState.Enter(owner);
    }
}
