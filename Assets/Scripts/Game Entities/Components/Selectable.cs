﻿using UnityEngine;


/// Класс, оборачивающий в себя прожекторы выделения и ховера
public class Selectable : MonoBehaviour 
{
    // **** Public Properties ****

    /// Ссылка на прожектор выделения
    /** 
    Предполагается, что он приаттачен к потомку этого гейм обжекта
    */
    public Projector SelectionCircle
    {
        get { return selectionCircle; }
        private set { selectionCircle = value; }
    }

    [SerializeField]
	private Projector selectionCircle;

    /// Ссылка на прожектор ховера
    /** 
    Предполагается, что он приаттачен к потомку этого гейм обжекта
    */
    public Projector HoverCircle
    {
        get { return hoverCircle; }
        private set { hoverCircle = value; }
    }

    [SerializeField]
    private Projector hoverCircle;
    

    // **** Unity Methods ****

    /// Метод Unity
    /**
    Класс регистрируется у SelectionController'а при Start'е
    */
    void Start()
    {
        if (SelectionCircle == null)
        {
            SelectionCircle = transform.Find("Selection Circle")
                .GetComponent<Projector>();
        }

        if (HoverCircle == null)
        {
            HoverCircle = transform.Find("Hover Circle")
                .GetComponent<Projector>();
        }

        SelectionController.RegisterSelectable(this);
    }

    /// Метод Unity
    /**
    Класс дерегистрируется у SelectionController'а при Ondestroy'е
    */
    void OnDestroy()
    {
        SelectionController.DeregisterSelectable(this);
    }
}
