﻿using UnityEngine;
using UnityEngine.UI;


/// Класс, отрисовывающий информацию об акторе
/**
Должен отрисовывать имя, текущее здоровье, прогресс текущего действия (для 
некоторых акторов)
*/
[RequireComponent(typeof(Destructible))]
public class UnitInfoController : MonoBehaviour 
{
    // **** Private Fields ****

    /// Ссылка на Destructible, здоровье которого отображается
    [SerializeField]
    private Destructible actorDestr;

    /// Ссылка на Canvas, где отображаются UI элементы
    [SerializeField]
    private Canvas canvas;

    /// Префаб панели, которая отображает параметры актора
    [SerializeField]
    private GameObject panelPrefab;
    
    /// Число показывающее насколько выше юнита нужно отрисовывать панель
    [SerializeField]
    private float panelOffset = 1f;

    /// Объект панели, которая отображает параметры актора
    [SerializeField]
    private GameObject infoPanel;

    /// Текстовое поле, где отображается имя  
    private Text nameText;

    /// Зачем вообще отрисовывать имя?
    [SerializeField]
    private bool shouldDrawName = false;

    /// Слайдер, отображающий текущее здоровье
    private Slider healthSlider;

    /// Слайдер, отображающий прогресс текущего действия
    private Slider progressSlider;

    /// Слайдер прогресса отрисовывается только для некоторых типов акторов
    [SerializeField]
    private bool shouldDrawProgressSlider = false;

    /// Делегат для функции возвращающей float'ы
    delegate float FloatValue();

    /// Сюда подставляется нужный метод конкретного актора и вызывается для
    /// определения текущего значения слайдера прогресса
    private FloatValue CurrentProgress;
    
    /// Сюда подставляется нужный метод конкретного актора и вызывается для
    /// определения максимального значения слайдера прогресса
    private FloatValue MaxProgress;

    /// Делегат для функции возвращающей bool'ы
    delegate bool BoolValue();

    /// Запоминает, что прогресс слайдер показывается только на время 
    /// строительства здания
    private BoolValue CanRemoveConstructionProgressSlider = 
        () => { return false; };


    // **** Unity Methods ****

    /// Метод Unity
    void Start()
    {
        actorDestr = GetComponent<Destructible>();

        if (canvas == null)
        {
            canvas = FindObjectOfType<Canvas>();
        }

        if (panelPrefab == null)
        {
            Debug.LogError("UnitInfoController::Start : panelPrefab reference " 
                + "is null. Have to disable this component.");
            enabled = false;

            return;
        }

        infoPanel = Instantiate<GameObject>(panelPrefab);
        infoPanel.transform.SetParent(canvas.transform, false);

        nameText = infoPanel.GetComponentInChildren<Text>();
        healthSlider = infoPanel.transform.Find("Health Slider")
            .GetComponent<Slider>();
        progressSlider = infoPanel.transform.Find("Progress Slider")
            .GetComponent<Slider>();

        ReasonAboutProgressSlider();
        ReasonAboutNameText();

        var worldPos = new Vector3(transform.position.x
            , transform.position.y + panelOffset, transform.position.z);
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        infoPanel.transform.position = screenPos;
    }

    /// Метод Unity
    void Update()
    {
        healthSlider.value = actorDestr.Health / (float) actorDestr.MaxHealth;

        if (shouldDrawProgressSlider)
        {
            if (CanRemoveConstructionProgressSlider())
            {
                shouldDrawProgressSlider = false;
                RemoveProgressSlider();
            }

            progressSlider.value = CurrentProgress() / MaxProgress();
        }

        var worldPos = new Vector3(transform.position.x
            , transform.position.y + panelOffset, transform.position.z);
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        infoPanel.transform.position = screenPos;
    }

    /// Метод Unity. Нужно удалить созданный Ui элемент при удалении объекта
    void OnDestroy()
    {
        Destroy(infoPanel);
    }


    // **** Private Methods ****

    /// Решает нужно ли отрисовывать слайдер прогресса и если да, то какие 
    /// показатели брать
    private void ReasonAboutProgressSlider()
    {
        if (shouldDrawProgressSlider)
        {
            TryToFindProgressingBehaviour();
        }

        if (!shouldDrawProgressSlider)
        {
            RemoveProgressSlider();
        }
    }

    private void TryToFindProgressingBehaviour()
    {
        // Попытаемся найти класс, у которого есть что отображать в прогресс
        // слайдере
        var attackingBehaviour = GetComponent<Attacker>();
        var unitProducingBehaviour = GetComponent<UnitProducerBehaviour>();
        var buildingBehaviour = GetComponentInChildren<BuildingBehaviour>();

        if (attackingBehaviour != null 
            && buildingBehaviour != null) // Если это, например, башня
        {
            InitTowerlike(attackingBehaviour, buildingBehaviour);
        }
        else if (unitProducingBehaviour != null)
        {
            InitUnitProducingBuilding(unitProducingBehaviour);
        }
        else if (buildingBehaviour != null)
        {
            InitOnlyWhileConstruction(buildingBehaviour);
            if (CanRemoveConstructionProgressSlider())
                shouldDrawProgressSlider = false;
        }
        else if (attackingBehaviour != null)
        {
            InitUnit(attackingBehaviour);
        }
        else
        {
            shouldDrawProgressSlider = false;
        }
    }


    private void ReasonAboutNameText()
    {
        if (!shouldDrawName)
        {
            nameText.gameObject.SetActive(false);
        }
        else
        {
            nameText.text = actorDestr.name;
        }
    }

    private void InitTowerlike(Attacker attackingBehaviour
        , BuildingBehaviour buildingBehaviour)
    {
        CurrentProgress = () => 
        {
            if (buildingBehaviour.IsUnderConstruction)
                return buildingBehaviour.CurrentConstructionTime;
            else
                return attackingBehaviour.AttackTimer;
        };
        MaxProgress = () => 
        {
            if (buildingBehaviour.IsUnderConstruction)
                return buildingBehaviour.WholeConstructionTime;
            else
                return attackingBehaviour.WholeAttackTime;
        };
    }

    private void InitUnit(Attacker attackingBehaviour)
    {
        CurrentProgress = () => { return attackingBehaviour.AttackTimer; };
        MaxProgress = () => { return attackingBehaviour.WholeAttackTime; };
    }

    private void InitUnitProducingBuilding(
        UnitProducerBehaviour unitProducingBehaviour)
    {
        CurrentProgress = () => 
        {
            if (unitProducingBehaviour.IsUnderConstruction)
                return unitProducingBehaviour.CurrentConstructionTime;
            else
                return unitProducingBehaviour.CurrentCreationTime;
        };
        MaxProgress = () => 
        {
            if (unitProducingBehaviour.IsUnderConstruction)
                return unitProducingBehaviour.WholeConstructionTime;
            else
                return unitProducingBehaviour.WholeCreationTime;
        };
    }

    private void InitOnlyWhileConstruction(BuildingBehaviour buildingBehaviour)
    {
        CanRemoveConstructionProgressSlider = 
            () => { return !buildingBehaviour.IsUnderConstruction; };
        CurrentProgress = 
            () => { return buildingBehaviour.CurrentConstructionTime; };
        MaxProgress = 
            () => { return buildingBehaviour.WholeConstructionTime; };
    }

    private void RemoveProgressSlider()
    {
        Vector3 offsetBetweenHealthAndProgress = 
            healthSlider.transform.position 
            - progressSlider.transform.position;

        progressSlider.gameObject.SetActive(false);
        healthSlider.transform.position -= offsetBetweenHealthAndProgress;
        nameText.transform.position -= offsetBetweenHealthAndProgress;
    }
}
