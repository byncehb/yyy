﻿using UnityEngine;


/// Вращает трансформ меча в соответствии с фазой атаки
public class SwordRotator : MonoBehaviour 
{
    // **** Private Fields ****

    /// Ссылка на трансформ меча
    [SerializeField]
    private Transform sword;

    /// Ссылка на атакующую компоненту
    private Attacker attacker;

    /// Угол максимального наклона меча
    [SerializeField]
    private float rotationAngle;

    /// Изначальное положение меча
    private Vector3 from;

    /// Крайнее положение меча
    private Vector3 to;


    // **** Unity Methods ****

    /// Метод Unity
    void Start() 
    {
        if (sword == null)
        {
            sword = transform.Find("Sword Handler");
        }
        
        attacker = GetComponent<Attacker>();
        from = sword.localEulerAngles;
        to = from + new Vector3(rotationAngle, 0, 0);
    }

    // Метод Unity
    void Update() {
        if (attacker.Attacking) {
            if (attacker.AttackTimer <= attacker.SwingTime) {
                sword.localEulerAngles = Vector3.Lerp(to, from
                    , attacker.AttackTimer / attacker.SwingTime);
            } else {
                sword.localEulerAngles = Vector3.Lerp(from, to
                    , (attacker.AttackTimer - attacker.SwingTime) 
                    / attacker.BackswingTime);
            }
        } else {
            sword.localEulerAngles = from;
        }
    }
}