﻿using UnityEngine;


/// Реализует цикл атаки юнита
public class Attacker : MonoBehaviour 
{
    // **** Public Properties ****

    /// Дальность атаки
    public float Range 
    {
        get { return range; }
        set 
        { 
            range = (value < 0) ? 0 : value;

            var proj = AttackRangeProj;
            if (proj != null)
            {
                proj.orthographicSize = range;
            }
        }
    }    

    [SerializeField] 
    private float range = 2f;

    /// Нужно ли отрисовывать круг дальности атаки
    public bool ShouldDrawAttackRange 
    { 
        get 
        { 
            return shouldDrawAttackRange; 
        } 
        set 
        {
            shouldDrawAttackRange = value;

            var proj = AttackRangeProj;
            if (proj != null)
            {
                proj.enabled = shouldDrawAttackRange;
            }
        } 
    }

    [SerializeField]
    private bool shouldDrawAttackRange;

    /// Время замаха
    public float SwingTime
    {
        get { return swingTime; }
    }

    [SerializeField] 
    private float swingTime = 0.6f;

    /// Время до готовности начать следующий замах 
    /** 
    Например, когда юнит перезаряжает ружьё после выстрела или тянет обратно к 
    себе меч, чтобы ещё раз замахнуться
     */
    public float BackswingTime
    {
        get { return backswingTime; }
    }

    [SerializeField]    
    private float backswingTime = 0.4f;

    /// Отражает текущий момент атаки
    /**
    Изменяется от 0 до WholeAttackTime
    */
    public float AttackTimer
    {
        get { return attackTimer; }
        private set { attackTimer = value; }
    }

    [SerializeField]
    private float attackTimer;

    /// Время полного цикла атаки
    /**
    Время замаха + время до следующего замаха
    */
    public float WholeAttackTime
    {
        get { return wholeAttackTime; }
        set { wholeAttackTime = value; }
    }

    [SerializeField] 
    private float wholeAttackTime;

    /// Находится ли в атакующем состоянии
    /**
    Определяет, активен ли цикл атаки
    */
    public bool Attacking { get; set; }

    /// Находится ли в фазе "перезарядки" атаки
    public bool IsBackswinging 
    { 
        get
        {
            return Attacking && !hasAttacked;
        }
    }


    // **** Private Fields ****

    /// Урон от атаки
    [SerializeField]
    private float damage = 5f;

    /// Произведена ли была атака в текущем цикле
    private bool hasAttacked;

    /// Ссылка на проектор круга дальности атаки
    private Projector AttackRangeProj
    {
        get
        {
            if (attackRangeProj == null)
            {
                Transform found = transform.Find("Attack Circle");
                attackRangeProj = found == null ? null 
                    : found.GetComponent<Projector>();
            }

            return attackRangeProj;
        }
    }

    [SerializeField]
    private Projector attackRangeProj;


    // **** Public Methods ****

    /// Готов ли объект нанести урон
    public bool ReadyToDealDamage()
    {
        if (Attacking && !hasAttacked && AttackTimer >= SwingTime)
        {
            hasAttacked = true;
            return true;
        }

        return false;
    }

    /// Наносит урон цели
    /**
    \param target Цель атаки
    */
    public void DealDamage(Destructible target)
    {
        target.TakeDamage(damage);
    }


    // **** Unity Methods ****

    /// Метод Unity
    void Start()
    {
        WholeAttackTime = SwingTime + BackswingTime;
    }

    /// Метод Unity
    /**
    Определяет текущую фазу цикла атаки. Обновляет таймеры
    */
    void Update()
    {
        if (Attacking || AttackTimer > SwingTime)
        {
            AttackTimer += Time.deltaTime;
        }

        if (AttackTimer > WholeAttackTime ||
            (!Attacking && AttackTimer < SwingTime))
        {
            AttackTimer = 0f;
            hasAttacked = false;
        }
    } 

    /// Метод Unity
    /**
    Вызывается при изменении свойств. Следит, чтобы при обновлении показателей
    отрисовки круга атаки из редактора, отрисовка изменялась
    */
    void OnValidate()
    {
        var proj = AttackRangeProj;
        if (proj != null)
        {
            proj.orthographicSize = Range;
            proj.enabled = ShouldDrawAttackRange;
        }
    }
}
