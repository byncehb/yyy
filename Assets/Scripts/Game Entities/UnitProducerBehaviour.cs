using UnityEngine;
using UnityEngine.AI;


/// Класс, описывающий здание, создающего юнитов по таймеру
[RequireComponent(typeof(NavMeshObstacle))]
public class UnitProducerBehaviour : BuildingBehaviour 
{
    // **** Public Properties ****

    /// Производит ли в данный момент здание юнитов
    public bool IsCreatingUnits 
    {
        get { return isCreatingUnits; }
        set { isCreatingUnits = value; }
    }

    [SerializeField] 
    private bool isCreatingUnits = true;

    /// Возвращает текущее значение таймера создания юнитов
    public float CurrentCreationTime { get; private set; }

    /// Возвращает значения таймера, при котором юнит должен создаться
    public float WholeCreationTime 
    { 
        get { return wholeUnitCreationTime; } 
    }

    [SerializeField] 
    private float wholeUnitCreationTime;


    // **** Private Fields ****

    /// Название префаба создаваемого юнита
    [SerializeField] 
    private string unitPrefabName;

    /// Место, где появляются создаваемые юниты
    /**
    Может быть задана в инспекторе. По умолчанию выбирается точка рядом со 
    зданием в направлении вражеского замка
    */
    [SerializeField] 
    private Transform spawnTransform; 

    /// Точка, где появляются создаваемые юниты
    private Vector3 spawnPoint;

    /// Направление, с которым появляются создаваемые юниты
    private Quaternion spawnRotation;


    // **** Unity Methods ****

    /// Метод Unity
    protected override void Start() 
    {
        base.Start();

        if (spawnTransform == null)
        {
            var found = transform.Find("Spawn Point");
            spawnTransform = found == null ? null : found.transform;
        }
    }

    /// Метод Unity
    protected override void Update() 
    {
        base.Update();

        if (!IsUnderConstruction && IsCreatingUnits)
        {
            CurrentCreationTime += Time.deltaTime;
            if (CurrentCreationTime >= WholeCreationTime)
            {
                SetSpawnPoint();

                Player.CreateUnit(unitPrefabName
                    , spawnPoint
                    , spawnRotation);

                CurrentCreationTime = 0f;
            }
        }
    }

    
    // **** Private Methods ****

    /// Находит точку и направление создания юнита
    private void SetSpawnPoint()
    {
        if (spawnTransform != null)
        {
            spawnPoint = spawnTransform.position;
            spawnRotation = spawnTransform.rotation;
        }
        else
        {
            Vector3 toEnemyCastle = Player.EnemyCastlePosition 
                - transform.position;
            float x = toEnemyCastle.x;
            float z = toEnemyCastle.z;
            Vector3 toEnemyCastleClamped = Mathf.Abs(x) > Mathf.Abs(z) ?
                new Vector3(1 * Mathf.Sign(x), 0, 0)
                : new Vector3(0, 0, 1 * Mathf.Sign(z));

            spawnPoint = transform.position 
                + (Radius + 1) * toEnemyCastleClamped;
            spawnRotation = Quaternion.Euler(toEnemyCastle);
        }
    }
}
