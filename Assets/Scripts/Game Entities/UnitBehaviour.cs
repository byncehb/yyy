using UnityEngine;
using UnityEngine.AI;


/// Описывает подвижного актора-юнита
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(NavMeshObstacle))]
public class UnitBehaviour : ActorBehaviour 
{
    // **** Public Properties ****

    /// Размер юнита для поиска пути
    public override float Radius
    {
        get 
        { 
            if (agent == null)
            {
                agent = GetComponent<NavMeshAgent>();
            }

            return agent.radius; 
        }
    }

    /// Расстояние до цели, на котором следует остановиться
    public float StoppingDistance { get; set; }

    /// Указывает актору не двигаться к цели, а только поворачиваться по 
    /// направлению к ней
    public bool ControlOnlyRotation { get; set; }


    // **** Private Fields ****

    /// Ссылка на агента навигации
    private NavMeshAgent agent;

    /// Ссылка на препятствие, в которое этот актор превращается, когда
    /**
    По умолчанию эта компонента выключена!
    */
    private NavMeshObstacle obstacle;

    /// Актор, к которому осуществляется движение
    private ActorBehaviour target;

    /// Точка, к которой осуществляется движение
    private Vector3 TargetPos
    {
        get 
        {
            return target == null ? targetPos : target.transform.position;
        }
    }

    /// Флаг, сигнализирует, что актор имеет цель
    private bool hasTarget;

    /// Используется, если target == null
    private Vector3 targetPos;

    /// Радиус цели, к которой осуществляется движение
    private float TargetRadius
    {
        get
        {
            return target == null ? 0f : target.Radius;
        }
    }

    /// Запомненная старая позиция цели, чтобы обновлять путь, если цель уйдет
    /// слишком далеко
    private Vector3 pathTargetPos;

    /// Расстояние в квадрате, на которое должна удалиться цель от старой 
    /// позиции, чтобы путь был пересчитан
    [SerializeField]
    private float epsSqr = 0.01f;

    /// Точка, на краю коллайдера цели к которой юнит движется
    private Vector3 pointOnEdgeOfTarget;

    /// Близжайшая к pointOnEdgeOfTarget точка на навмеше
    /**
    К этой точке на самом деле движется объект
    */
    private Vector3 targetPosOnNavMesh;

    /// Таймер пересчета пути
    private float recalculatePathTimer = 0.2f;

    /// Значение таймера, когда следует пересчитать путь
    [SerializeField]
    private float timeWhenRecalculatePath = 0.2f;

    private bool shouldEnableAgentNextFrame;


    // **** Public Methods ****

    public void SetTarget(ActorBehaviour target)
    {
        if (hasTarget && this.target != null && this.target == target)
        {
            return;
        }

        hasTarget = true;
        this.target = target;
        targetPos = Vector3.zero;
        pathTargetPos = targetPos;
        recalculatePathTimer = timeWhenRecalculatePath;
        UpdatePoints();
    }

    public void SetTarget(Vector3 point)
    {        
        if (hasTarget && targetPos == point)
        {
            return;
        }

        hasTarget = true;
        target = null;
        targetPos = point;
        pathTargetPos = targetPos;
        recalculatePathTimer = timeWhenRecalculatePath;
        UpdatePoints();
    }

    public void EraseTarget()
    {
        hasTarget = false;
        target = null;
        targetPos = Vector3.zero;
    }

    // **** Unity Methods ****

    /// Метод Unity
    protected override void Start() 
    {
        base.Start();
        
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }

        obstacle = GetComponent<NavMeshObstacle>();

        obstacle.enabled = false;
        agent.enabled = true;
    }

    /// Метод Unity
    protected override void Update()
    {
        /* 
        Про то, как движется актор:
        Актору задается либо другой актор, либо точка в качестве цели. Так как 
        цель может быть объемной и вырезать кусок из навмеша, нужно найти 
        близжайшую к цели точку на навмеше при том так, чтобы она была в
        направлении актора. К этой найденной точке и будет двигаться актор. 
        Двигаться он будет пока не достигнет радиуса атаки до крайней точки цели
        (важно, что эта крайняя точка не обязательно та же что и точка навмеша. 
        Она скорее всего будет ближе к центру цели, чем точка навмеша).
        */
            
        agent.enabled = agent.enabled || shouldEnableAgentNextFrame;

        if (hasTarget)
        {
            recalculatePathTimer += Time.deltaTime;

            // Оказались достаточно близко к реальной текущей позиции цели
            if (IsInRange() || ControlOnlyRotation)
            {
                // Остановились
                agent.enabled = false;
                obstacle.enabled = true;

                // Может оказаться, что актор не до конца повернут к цели
                // В этом случае нужно вручную довращать его
                const float rotationAccuracy = 0.98f;
                Vector3 toTarget = (TargetPos - transform.position).normalized;
                if (Vector3.Dot(transform.forward, toTarget)
                    < rotationAccuracy)
                {
                    RotateTowardsManually(toTarget);
                }
            }
            else
            {
                obstacle.enabled = false;
                // После выключения обстакла должен пройти 1 кадр прежде чем
                // включаем агента, а то его выкинет с вырезанной обстаклом 
                // части навмеша
                shouldEnableAgentNextFrame = !agent.enabled;

                if (agent.enabled
                    && (recalculatePathTimer >= timeWhenRecalculatePath
                        || (pathTargetPos - TargetPos).sqrMagnitude >= epsSqr))
                {
                    UpdatePath();
                    recalculatePathTimer = 0f;
                }
            }
        }
    }


    // **** Private Methods ****

    private void UpdatePath()
    {
        UpdatePoints();
        MoveTo(targetPosOnNavMesh);
        pathTargetPos = TargetPos;
    }

    /// Указывает двигаться к точке
    /**
    \param point Точка назначения
    */
    private void MoveTo(Vector3 point)
    {
        if (!agent.pathPending)
        {
            agent.isStopped = false;
            agent.destination = point;
            Debug.DrawRay(agent.destination, Vector3.up * 20f, Color.yellow, 2f);
            /*if (!agent.SetDestination(point))
            {
                Debug.Log("<UnitBehaviour::MoveTo(Vector3)> Can't set" + 
                    " destination to " + point);
            }*/
        }
    }

    private void RotateTowardsManually(Vector3 direction)
    {
        Quaternion lookRotation = Quaternion.LookRotation(
            new Vector3(direction.x, 0, direction.z));
        float rotationAngle = Quaternion.Angle(transform.rotation
            , lookRotation);

        // Я не понимаю почему тут именно 
        // Time.deltaTime * agent.angularSpeed / rotationAngle
        // но это вроде работает
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation
            , Time.deltaTime * agent.angularSpeed / rotationAngle);

    }

    private bool IsInRange()
    {
        float dist = Vector3.Distance(transform.position, TargetPos);
        dist = target == null ? dist : dist - target.Radius;
        return dist <= StoppingDistance;
    }

    private void UpdatePoints()
    {
        pointOnEdgeOfTarget = TargetPos + (target == null ? 0f : target.Radius) 
            * ((transform.position - TargetPos).normalized);

        NavMeshHit hit;
        const float maxDistance = 2f;
        if (NavMesh.SamplePosition(pointOnEdgeOfTarget, out hit, maxDistance
            , NavMesh.AllAreas))
        {
            targetPosOnNavMesh = hit.position;
        }
        else
        {
            Debug.Log("<UnitBehaviour::UpdatePoints()> Can't find point on " 
                + "navmesh close to " + pointOnEdgeOfTarget);
            // Раз не смогли найти, то положим идти просто к координатам цели,
            // пусть юнити сам ищет ближайшую точку
            targetPosOnNavMesh = TargetPos;
        }
    }
}