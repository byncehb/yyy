﻿using UnityEngine;
using UnityEngine.AI;


/// Описывает простое здание, которое ничего не делает
[RequireComponent(typeof(NavMeshObstacle))]
public class BuildingBehaviour : ActorBehaviour 
{
    // **** Public Properties ****

    /// Определяет строится ли ещё это здание
    public bool IsUnderConstruction 
    {
        get { return isUnderConstruction; }
        private set { isUnderConstruction = value; } 
    }

    [SerializeField] 
    private bool isUnderConstruction = true;

    /// Возвращает текущее значение таймера постройки
    public float CurrentConstructionTime { get; private set; }

    /// Возвращает значения таймера, при котором здание считается построенным
    public float WholeConstructionTime
    {
        get { return wholeUnderConstructionTime; }
    }

    [SerializeField] 
    private float wholeUnderConstructionTime;

    /// Определяет размер объекта при поиске пути используя NavMeshObstacle
    public override float Radius
    {
        get 
        { 
            if (obst == null)
                obst = GetComponent<NavMeshObstacle>();

            return obst.radius; 
        }
    }


    // **** Private Fields ****

    /// Ссылка на объект, отвечающий за поиск пути
    /**
    Должен делать Carve с NavMesh'ем и быть формы капсулы по умолчанию c 
    радиусом в 2
    */
    private NavMeshObstacle obst;


    // **** Unity Methods ****

    /// Метод Unity
    protected override void Start () 
    {
        base.Start();

        if (obst == null)
            obst = GetComponent<NavMeshObstacle>();

        ReasonAboutConstruction();
    }
    
    // Метод Unity. Тут обновляется таймер строительства
    protected override void Update () 
    {
        if (IsUnderConstruction)
        {
            CurrentConstructionTime += Time.deltaTime;
            Destructible.Health += (Destructible.MaxHealth * 
                Time.deltaTime) / WholeConstructionTime;

            if (CurrentConstructionTime >= WholeConstructionTime)
            {
                IsUnderConstruction = false;
            }
        }   
    }


    // **** Private Methods ****

    /// Решает нужно ли начинать процесс строительства здания или сразу создать 
    /// готовое
    private void ReasonAboutConstruction()
    {
        if (IsUnderConstruction)
        {
            Destructible.Health = 1;
        }
    }
}
