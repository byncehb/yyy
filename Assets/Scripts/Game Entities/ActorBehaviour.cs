﻿using UnityEngine;


/// Класс, описывающий минимальную игровую сущность
[RequireComponent(typeof(Destructible))]
[RequireComponent(typeof(UnitInfoController))]
[RequireComponent(typeof(Selectable))]
abstract public class ActorBehaviour : MonoBehaviour 
{
    // **** Public Properties ****

    /// Определяет размер объекта при поиске пути
    /** Абстрактное, потому что способ определения размера зависит от 
    класса, реализующего поиск пути, который может различаться у разных акторов
    */
    public abstract float Radius { get; }

    /// Ссылка на игрока, владеющего юнитом
    public PlayerController Player 
    { 
        get 
        { 
            if (player == null)
            {
                GameObject found = Utils.FindParentWithTag(gameObject
                    , "Player");
                player = found == null ? null 
                    : found.GetComponent<PlayerController>();
            }
            
            return player; 
        }
        set { player = value; } 
    }

    [SerializeField] 
    private PlayerController player;

    /// Ссылка на Destructible этого объекта
    public Destructible Destructible { get; private set; }

    /// Дальность зрения объекта
    public float VisionRange
    {
        get { return visionRange; }
        set 
        { 
            visionRange = value < 0 ? 0 : value;

            var proj = VisionRangeProj; 
            if (proj != null)
            {
                proj.orthographicSize = visionRange;
            }
        }
    }

    [SerializeField] 
    private float visionRange = 5f;

    /// Следует ли отрисовывать круг поля зрения
    public bool ShouldDrawVisionRange 
    {
        get 
        { 
            return shouldDrawVisionRange;
        }
        set 
        { 
            shouldDrawVisionRange = value;

            var proj = VisionRangeProj;
            if (proj != null)
            {
                proj.enabled = shouldDrawVisionRange;
            }
        }
    }

    [SerializeField]
    private bool shouldDrawVisionRange = true;


    // **** Private Fields ****

    /// Проектор, который отрисовывает круг поля зрения объекта
    private Projector VisionRangeProj
    {
        get 
        {
            if (visionRangeProj == null)
            {
                Transform found = transform.Find("Vision Circle");
                visionRangeProj = found == null ? null 
                    : found.GetComponent<Projector>();
            }

            return visionRangeProj;
        }
    }

    [SerializeField]
    private Projector visionRangeProj;


    // **** Public Methods ****

    /// Инициализация объекта после Instantiate
    /**  Должен обязательно вызываться после Instantiate, если объект будет
    активным
    \param player Ссылка на игрока, которому созданный объект будет принадлежать
    */
    public void Init(PlayerController t_player)
    {
        if (t_player == null)
        {
            GameObject found = Utils.FindParentWithTag(gameObject, "Player");
            t_player = found == null ? null 
                : found.GetComponent<PlayerController>();
            if (t_player == null)
            {
                Debug.LogError("ActorBehabiour::Init : Passed null as player " 
                    + "parameter and can't find any Player tagged object." 
                    + " Destroying this game object.");
                Destroy(gameObject);

                return;
            }
        }

        Player = t_player;
    }


    // **** Unity Methods ****

    /// Метод Unity. Проверяется выставлены ли поля, для которых тяжело найти  
    /// способ инициализации по умолчанию, и кешируются обязательные компоненты 
    /// класса
    protected virtual void Start()
    {
        if (Player == null)
        {
            Debug.LogError("<ActorBehabiour::Start> : Player reference is " 
                + "null and can't find any Player tagged object." 
                + " Disabling this game object.");

            gameObject.SetActive(false);
        }

        Destructible = GetComponent<Destructible>();
        SetColor();
    }

    /// Метод Unity
    protected abstract void Update();

    /// Метод Unity. Вызывается при обновлении значений свойств в редакторе
    void OnValidate()
    {
        var proj = VisionRangeProj;
        if (proj != null)
        {
            proj.orthographicSize = VisionRange;
            proj.enabled = ShouldDrawVisionRange;
        }
    }


    // **** Private Methods ****

    /// Окрашивает объект в соответствии с цветом игрока
    private void SetColor()
    {
        var color = Player.Color;
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
        {
            if (r.CompareTag("Colorable"))
            {
                r.material.color = color;
            }
        }
    }
}
