﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Projector/Circle" 
{
	Properties 
	{
		_Color("Color", Color) = (1,1,1,1)
		_Thickness("Thickness", Range(0.0, 0.5)) = 0.025
	}
	SubShader 
	{
		Pass 
		{
			//Blend SrcAlpha OneMinusSrcAlpha
			Blend SrcAlpha One
			CGPROGRAM
			// Uses the Labertian lighting model
			//#pragma surface surf Lambert
			#pragma vertex vert             
			#pragma fragment frag
		
			struct vertInput 
			{
			 float4 pos : POSITION;
			 float2 texcoord : TEXCOORD0;
			};  
			 
			struct vertOutput 
			{
			 float4 pos : SV_POSITION;
			 float2 posProj : TEXCOORD0;
			};

			float4x4 unity_Projector;
			float4x4 unity_ProjectorClip;

			float _Thickness;
			fixed4 _Color;

			vertOutput vert(vertInput input) {
			 vertOutput o; 
			 o.pos = UnityObjectToClipPos(input.pos);
			 o.posProj = mul(unity_Projector, input.pos).xy - fixed2(0.5, 0.5);
			 return o;
			}

			half4 frag(vertOutput o) : COLOR {

				float width = _Thickness * length(unity_Projector._m00_m10_m20);// / unity_OrthoParams.y * 2;// / unity_OrthoParams.x;
				float radius = 0.5 - width;
			    float dist =  length(o.posProj);
				float t = 1.0 + smoothstep(radius, radius + width, dist) 
				            - smoothstep(radius - width, radius, dist);
				return fixed4(_Color.rgb, _Color.a * lerp(1.0, 0.0, t));

				/*if (dist < radius + width && dist > radius - width)
				{
					return fixed4(1.0, 1.0, 1.0, 1.0);
				}
				else
				{
					return fixed4(1.0, 1.0, 1.0, 0.0);
				}*/
			}

			ENDCG
		}
	}
}
